
export let spinnersLoading = {
    batLoading: () => {
        document.getElementById("loading").style.display = "block";
        document.getElementById("content-body").style.display = "none";
    },
    tatLoading: () => {
        document.getElementById("loading").style.display = "none";
        document.getElementById("content-body").style.display = "block";
    }
};
