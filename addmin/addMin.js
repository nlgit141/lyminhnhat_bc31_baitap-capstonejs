import { productsController } from "../controller/productsController.js";
import { phoneServices } from "../services/phoneServices.js";
import { spinnersLoading } from "../services/spinnersLoading.js";
import { kiemTraRong } from "./validation/validation.js";


let dataPhone = [];
let iphoneList = [];
let samsungList = [];
let idSpEdited = null;


let kiemTraThongTinNhap = () => {
    let SP = productsController.layThongTinSanPhamTuForm();
    let valid = true;
    // kiểm tra rỗng:
    valid &= kiemTraRong(SP.id, "#errMaSanPham", "ID SP ")
        & kiemTraRong(SP.name, "#errTenSP", "tên SP ")
        & kiemTraRong(SP.price, "#errGiaSP", "Giá SP ")
        & kiemTraRong(SP.desc, "#errMotaSP", "Mô tả ")
        & kiemTraRong(SP.img, "#errHinhAnhSP", "Link SP ")
        & kiemTraRong(SP.type, "#errHangSP", "Hãng SP ");

    if (!valid) {
        return;
    }
};



let offMessErr = () => {
    let ab = document.getElementsByTagName("input");
    for (let i = 0; i < ab.length; i++) {
        let item = ab[i];
        item.addEventListener("change", function () {
            let cd = document.getElementsByTagName("span");
            for (let j = 0; j < cd.length; j++) {
                let itemb = cd[j];
                if (!itemb == "") {
                    itemb.style.display = "none";
                }
            }
        });
    }
};
offMessErr();


// render table phone
let renderCartPhone = (list) => {
    let contentHTML = "";
    list.map((item) => {
        let { id, img, price, name, desc } = item;
        (contentHTML += `
                    <tr>
                        <td>${id}</td>
                        <td>${name}</td>
                        <td>$${price}</td>
                        <td>${desc}</td>
                        <td><img src="${img}" style="width:30px"/></td>
                        <td>
                        <button onclick="removeSP(${id})" class="btn btn-danger">xóa</button>
                        <button onclick="editSP(${id})" class="btn btn-warning">xem</button>
                        </td>
                    </tr>`);
    }); document.getElementById("table-cart").innerHTML = contentHTML;
};

//lấy Data-axios
let getDataPhoneService = () => {
    spinnersLoading.batLoading();
    phoneServices.getDataPhone()
        .then((res) => {
            dataPhone = res.data;

            // phân loại hãng điện thoại;
            let item = dataPhone[0];
            console.log('dataPhone2: ', dataPhone);
            for (let index = 0; index < dataPhone.length; index++) {
                item = dataPhone[index];

                if (item.type === "iphone" || item.type === "Iphone") {
                    iphoneList.push(item);
                }
                if (item.type === "samsung" || item.type === "Samsung") {
                    samsungList.push(item);
                }
            } spinnersLoading.tatLoading();
            renderCartPhone(dataPhone);
        }).catch((err) => {
            spinnersLoading.tatLoading();
            return alert("không lấy được dữ liệu");
        });
};
getDataPhoneService();

// search tìm kiếm điện thoại theo hãng
document.getElementById("key-work").addEventListener("change", function () {
    let keyWork = document.getElementById("key-work").value;
    let keyNew = keyWork.toLowerCase();
    if (keyNew === "iphone") {
        renderCartPhone(iphoneList);
    }
    else if (keyNew === "samsung") {
        renderCartPhone(samsungList);
    } else {
        renderCartPhone(dataPhone);
    }
});


// xóa SP
let removeSP = (idSP) => {
    phoneServices.removeSP(idSP)
        .then((res) => {
            getDataPhoneService();
        })
        .catch((err) => {
        });
};
window.removeSP = removeSP;

// thêm mới sản phẩm
let addSP = () => {
    let SP = productsController.layThongTinSanPhamTuForm();
    spinnersLoading.batLoading();
    phoneServices.addSP(SP)
        .then((res) => {
            spinnersLoading.tatLoading();
            getDataPhoneService();
        }).catch((err) => {
            spinnersLoading.tatLoading();
        });
    kiemTraThongTinNhap();
}; window.addSP = addSP;


// sửa sản phẩm
let editSP = (idSP) => {
    spinnersLoading.batLoading();
    idSpEdited = idSP;
    phoneServices.layChiTietSP(idSP).then((res) => {
        spinnersLoading.tatLoading();
        productsController.showThongTinLenForm(res.data);
    }).catch((err) => {
        spinnersLoading.tatLoading();
    });
    document.getElementById("masanpham").disabled = true;
    document.getElementById("capnhatsanpham").style.display = "inline-block";
    document.getElementById("themmoisanpham").style.display = "none";
};
window.editSP = editSP;




// cập nhật sản phẩm
let capNhatSP = () => {
    let SP = productsController.layThongTinSanPhamTuForm();
    spinnersLoading.batLoading();
    let newSP = { ...SP, id: idSpEdited };
    phoneServices.capNhatSP(newSP)
        .then((res) => {
            spinnersLoading.tatLoading();
            productsController.showThongTinLenForm({
                id: "",
                name: "",
                price: "",
                img: "",
                desc: "",
                type: ""
            });
            getDataPhoneService();
        })
        .catch((err) => {
            spinnersLoading.tatLoading();
        });
    document.getElementById("capnhatsanpham").style.display = "none";
    document.getElementById("themmoisanpham").style.display = "inline-block";
    document.getElementById("masanpham").disabled = false;
};
window.capNhatSP = capNhatSP;

window.onload = document.getElementById("capnhatsanpham").style.display = "none";