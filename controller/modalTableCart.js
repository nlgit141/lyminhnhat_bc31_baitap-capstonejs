

let modaltable = document.getElementById("modaltable");
let btnTableCart = document.getElementById("btn-table-cart");
let closeTableCart = document.getElementsByClassName("close")[0];



// Khi button được click thi mở Modal
btnTableCart.onclick = function () {
    modaltable.style.display = "block";
};

// Khi closeTableCart được click thì đóng Modal
closeTableCart.onclick = function () {
    modaltable.style.display = "none";
};

// Khi click ngoài Modal thì đóng Modal
window.onclick = function (event) {
    if (event.target == modaltable) {
        modaltable.style.display = "none";
    }
};
