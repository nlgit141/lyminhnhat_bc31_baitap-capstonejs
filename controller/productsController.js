export let productsController = {
    layThongTinSanPhamTuForm: () => {
        let masanpham = document.getElementById("masanpham").value;
        let sanpham = document.getElementById("sanpham").value;
        let dongia = document.getElementById("dongia").value;
        let hinhanh = document.getElementById("hinhanh").value;
        let mota = document.getElementById("mota").value;
        let hang = document.getElementById("hang").value;

        let sanPham = {
            id: masanpham,
            name: sanpham,
            price: dongia,
            img: hinhanh,
            desc: mota,
            type: hang
        };
        return sanPham;
    },
    showThongTinLenForm: (sanPham) => {
        document.getElementById("masanpham").value = sanPham.id;
        document.getElementById("sanpham").value = sanPham.name;
        document.getElementById("dongia").value = sanPham.price;
        document.getElementById("hinhanh").value = sanPham.img;
        document.getElementById("mota").value = sanPham.desc;
        document.getElementById("hang").value = sanPham.type;
    }

};    