import { phoneServices } from './services/phoneServices.js';
import { spinnersLoading } from './services/spinnersLoading.js';

let listPhone = [];
let iphoneList = [];
let samsungList = [];
let cartPhone = [];





// let renderListPhone = (list) => {
//     let contentHTML = "";
//     for (let i = 0; i < list.length; i++) {
//         let phone = list[i];
//         let contentItem =
//             `<div class="col-3">
//                 <div class="card" style="width: 18rem;">
//                     <img class="card-img-top" src="${phone.img}" alt="Card image cap">
//                         <div class="card-body">
//                             <h5 class="card-title">${phone.name}</h5>
//                             <p class="card-text">${phone.desc}</p>
//                             <button onclick="addToCart(${phone.id})" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>
//                         </div>
//                 </div>
//             </div>`;
//         contentHTML += contentItem;
//         document.getElementById("products").innerHTML = contentHTML;
//     }
// };


let renderListPhone = (list) => {
    let contentHTML = "";
    list.map((item) => {
        let { id, img, name, desc, price } = item;
        let content = `<div class="col-3 mt-4 item-cart">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="${img}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <span>Giá: $${price}</span>
                    <p class="card-text">${desc}</p>
                    <div class="d-flex justify-content-lg-between">
                        <div>
                        <button class="btn btn-warning">chi tiết</button>
                        </div>
                        <div>
                        <button onclick="addToCart(${id}),muaNgay()"  class="btn btn-dark">mua ngay</button>
                        </div>
                        <div>
                        <button onclick="addToCart(${id})" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></button></div>
                    </div>
                </div >
        </div >
    </div > `; contentHTML += content;
    });
    document.getElementById("products").innerHTML = contentHTML;
};


//lấy Data-axios
let getDataPhoneService = () => {
    spinnersLoading.batLoading();
    phoneServices.getDataPhone()
        .then((res) => {
            listPhone = res.data;
            // phân loại hãng điện thoại;
            let item = listPhone[0];
            for (let index = 0; index < listPhone.length; index++) {
                item = listPhone[index];
                if (item.type === "iphone" || item.type === "Iphone") {
                    iphoneList.push(item);
                }
                if (item.type === "samsung" || item.type === "Samsung") {
                    samsungList.push(item);
                }
                spinnersLoading.tatLoading();
            } renderListPhone(listPhone);
        }).catch((err) => {
            spinnersLoading.tatLoading();
            return alert("không lấy được dữ liệu");
        });
};
getDataPhoneService();


// lựa chọn điện thoại theo hãng
document.getElementById("select-phone").addEventListener("change", function () {
    let dienThoai = document.getElementById("select-phone").value;
    switch (dienThoai) {
        case "iphone": {
            renderListPhone(iphoneList);
            break;
        }
        case "samsung": {
            renderListPhone(samsungList); break;
        }
        default: renderListPhone(listPhone);
            break;
    }
});


// search tìm kiếm điện thoại theo hãng
document.getElementById("key-work").addEventListener("change", function () {
    let keyWork = document.getElementById("key-work").value;
    let keyNew = keyWork.toLowerCase();
    if (keyNew === "iphone") {
        renderListPhone(iphoneList);
    }
    else if (keyNew === "samsung") {
        renderListPhone(samsungList);
    } else {
        renderListPhone(listPhone);
    }
});

// sắp xếp điện thoại
document.getElementById("sort-phone").addEventListener("change", () => {
    let valueSelect = document.getElementById("sort-phone").value;

    listPhone.sort((a, b) => {
        if (valueSelect == "A-Z") {
            return a.name.localeCompare(b.name);
        } else if (valueSelect == "Z-A") {
            return b.name.localeCompare(a.name);
        } else if (valueSelect == "giagiam") {
            return b.price - a.price;
        }
        else if (valueSelect == "giatang") {
            return a.price - b.price;
        }
        else {
            return a.id - b.id;
        }
    });
    renderListPhone(listPhone);
});


//  render giỏ hàng
let renderCartPhone = () => {
    let contentHTML = "";
    cartPhone.map((item) => {
        let { id, img, price, name, desc, quantity, tongTien } = item;
        (contentHTML += `
                    <tr>
                        <td>${id}</td>
                        <td>${name}</td>
                        <td>$${price}</td>
                        <td>${desc}</td>
                        <td><img src="${img}" style="width:30px"/></td>
                        <td>
                        <button onclick="changeQuantity(${id}, ${-1})" class="btn btn-warning">-</button>
                        <span class=mx-2>${quantity}</span>
                        <button onclick="changeQuantity(${id}, ${+1})" class="btn btn-primary">+</button>
                        </td>
                        <td>
                        <span class="font-weight-bold">$${tongTien}</span>
                        </td>
                        <td>
                        <button onclick="removeCartPhone(${id})" class="btn btn-danger">xóa</button>
                        </td>
                    </tr>`);
    });
    document.getElementById("table-cart").innerHTML = contentHTML;

    let tinhTien = 0;
    let giohang = 0;
    cartPhone.forEach((item) => {
        return (tinhTien += (item.price * item.quantity), giohang += item.quantity);
    });
    document.getElementById("tongThanhToan").innerHTML = "Tổng thanh toán: $" + tinhTien;
    // hiển thị số lượng sản phẩm đang có trong giỏ hàng
    giohang == 0 ? ((document.getElementById("txt-soLuong").innerHTML = ""), (document.getElementById("modaltable").style.display = "none")) : (document.getElementById("txt-soLuong").innerHTML = giohang);

};
renderCartPhone(cartPhone);

// lấy data từ LocalStorage
let dataJSON = localStorage.getItem("keyCartPhone");
if (dataJSON !== null) {
    let arrCartPhone = JSON.parse(dataJSON);
    for (let i = 0; i < arrCartPhone.length; i++) {
        let item = arrCartPhone[i];
        cartPhone.push(item);
    } renderCartPhone(cartPhone);
}

// lưu LocalStorage
let luuLocalStorage = () => {
    let jsonCartPhone = JSON.stringify(cartPhone);
    localStorage.setItem("keyCartPhone", jsonCartPhone);
};

//thêm sản phẩm vào giỏ hàng: add to cart
let addToCart = (id) => {
    //tìm vị trí//
    let index = cartPhone.findIndex((item) => {
        return item.id == id;
    });

    if (index == -1) {
        //   nếu không tìm thấy trong cartPhone thì  tìm đối tượgn đã click trong listPhone
        const newData = listPhone.find((item) => item.id == id);

        if (newData) {
            // nếu tìm thấy sản phẩm đã click trong listPhone thì khai báo nó là 1 sản phẩm đước add vào giỏ hàng,và thêm biến số lương
            let newPhone = { ...newData, quantity: 1, tongTien: 0 };
            newPhone.tongTien = newPhone.tongTien + newPhone.price * 1;
            cartPhone.push(newPhone);
        }
    } else {
        //   tìm thấy trong cartPhone thì số lượng tăng
        cartPhone[index].quantity++;
        cartPhone[index].tongTien = cartPhone[index].price * cartPhone[index].quantity;
    }

    luuLocalStorage();
    renderCartPhone();
};
window.addToCart = addToCart;


let muaNgay = () => {
    document.getElementById("modaltable").style.display = "block";
};
window.muaNgay = muaNgay;


// Tăng giảm số lượng sản phẩm: increase or decrease the amount
let changeQuantity = (spID, step) => {
    let index = cartPhone.findIndex((item) => {
        return item.id == spID;
    });
    if (index !== -1) {
        cartPhone[index].quantity = cartPhone[index].quantity + step;

        cartPhone[index].tongTien = cartPhone[index].price * cartPhone[index].quantity;
    }
    if (cartPhone[index].quantity == 0) {
        cartPhone.splice(index, 1);
    }
    luuLocalStorage();
    renderCartPhone(cartPhone);
};
window.changeQuantity = changeQuantity;

// xóa sản phẩm trong giỏ hàng: delete cart phone
let removeCartPhone = (spID) => {
    let index = cartPhone.findIndex((item) => {
        return item.id = spID;
    });
    if (index !== -1) {
        cartPhone.splice(index, 1);
    }
    luuLocalStorage();
    renderCartPhone(cartPhone);
};
window.removeCartPhone = removeCartPhone;


// nhấn thanh toán reset mảng cart về 0
let thanhToan = () => {
    let resetCartPhone = [];
    cartPhone = resetCartPhone;
    renderCartPhone(cartPhone);
    luuLocalStorage();
};
window.thanhToan = thanhToan;






